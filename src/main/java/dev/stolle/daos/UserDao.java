package dev.stolle.daos;

import java.util.List;

import dev.stolle.models.*;

public interface UserDao {
	public Customer getCustomer(String userName, String password);
	public Employee getEmployee(String userName, String password);
	public List<Account> getCustomerAccounts(int userId);
	public Account getCustomerAccount(Customer customer, int accountId);
	public List<Customer> getAllCustomers();
}
