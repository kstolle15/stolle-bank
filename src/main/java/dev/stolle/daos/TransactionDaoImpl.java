package dev.stolle.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dev.stolle.models.Account;
import dev.stolle.models.AccountStatus;
import dev.stolle.models.AccountType;
import dev.stolle.models.Customer;
import dev.stolle.util.ConnectionUtil;

public class TransactionDaoImpl {
	private static Logger log = Logger.getRootLogger();
	
	public boolean applyForAccount(Customer customer, Account account) {
		String sql = "INSERT INTO Accounts (User_Id, Balance,Account_Type) VALUES (?,?,?)";
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement pStatement = conn.prepareStatement(sql);) {
			pStatement.setInt(1,customer.getUserId());
			pStatement.setDouble(2, account.getBalance());
			String type = "";
			if(account.getType() == AccountType.C) {
				type = "C";
			} else {
				type = "S";
			}
			pStatement.setString(3, type);
			pStatement.execute(); 
			log.info("Applied for new account: " + account.getAccountNumber() + " for user: " + customer.getUserName());
			return true;
		} catch (SQLException ex) {
			log.error("Error occurred applying for account: " + ex.getClass());
			ex.printStackTrace();
		}  
		
		return false;
	}
	
	public boolean approveAccount(Account account) {
		String sql = "UPDATE Accounts SET Status = ? WHERE Account_Id = ?";
		int numOfItemsUpdated = 0;
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement pStatement = conn.prepareStatement(sql);){
			String status = "APPROVED";
			pStatement.setString(1, status);
			pStatement.setInt(2, account.getAccountNumber());
			numOfItemsUpdated = pStatement.executeUpdate();
			if(numOfItemsUpdated == 1) {
				log.info("Successfully approved account status for account: " + account.getAccountNumber());
				return true;
			} else {
				log.error("Unsuccessfully approved account status for account: " + account.getAccountNumber());
				return false;
			}
		} catch (SQLException ex) {
			log.error("Error approving account: " + account.getAccountNumber() + ". " + ex.getClass());
			ex.printStackTrace();
		}
		
		return false;
	}
	
	public boolean rejectAccount(Account account) {
		String sql = "UPDATE Accounts SET Status = ? WHERE Account_Id = ?";
		int numOfItemsUpdated = 0;
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement pStatement = conn.prepareStatement(sql);){
			String status = "REJECTED";
			pStatement.setString(1, status);
			pStatement.setInt(2, account.getAccountNumber());
			numOfItemsUpdated = pStatement.executeUpdate();
			if(numOfItemsUpdated == 1) {
				log.info("Successfully rejected account for account: " + account.getAccountNumber());
				return true;
			} else {
				log.error("Unsuccessfully rejected account status for account: " + account.getAccountNumber());
				return false;
			}
		} catch (SQLException ex) {
			log.error("Error rejecting account: " + account.getAccountNumber() + ". " + ex.getClass());
			ex.printStackTrace();
		}
		
		return false;
	}
	
	public boolean updateBalance(Account account) {
		String sql = "UPDATE Accounts SET Balance = ? WHERE Account_Id = ?";
		int numOfItemsUpdated = 0;
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement pStatement = conn.prepareStatement(sql);) {
			pStatement.setDouble(1, account.getBalance());
			pStatement.setInt(2, account.getAccountNumber());
			numOfItemsUpdated = pStatement.executeUpdate();
			if(numOfItemsUpdated == 1) {
				log.info("Successfully updated account balance for account: " + account.getAccountNumber());
				return true;
			} else {
				log.error("Unsuccessfuly updated account balance for account: " + account.getAccountNumber());
				return false;
			}
		} catch (SQLException ex) {
			log.error("Error occurred updating account balance: " + ex.getClass());
			ex.printStackTrace();
		}
		
		return false;
	} 
	
	public List<Account> getPendingAccounts(){
		String sql = "SELECT * FROM Accounts WHERE Status = ?";
		String status = "PENDING_APPROVAL";
		ArrayList<Account> accounts = new ArrayList<Account>();
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement pStatement = conn.prepareStatement(sql);){
			pStatement.setString(1, status);
			ResultSet rs = pStatement.executeQuery();
			
			while(rs.next()) {
				Account account = new Account();
				account.setAccountNumber(rs.getInt("Account_Id"));
				account.setBalance(rs.getDouble("Balance"));
				String accStatus = rs.getString("Status");
				if(!accStatus.equals("PENDING_APPROVAL")) {
					log.error("Error retrieving pending account: " + account.getAccountNumber());
					return null;
				}
				account.setStatus(AccountStatus.PENDING_APPROVAL);
				String type = rs.getString("Account_Type");
				if (type.equals("C")) {
					account.setType(AccountType.C);
				} else {
					account.setType(AccountType.S);
				}
				accounts.add(account);
			}
		} catch (SQLException ex) {
			log.error("Error retrieving pending accounts. " + ex.getClass());
			ex.printStackTrace();
		}
		
		return accounts;
	}
}
