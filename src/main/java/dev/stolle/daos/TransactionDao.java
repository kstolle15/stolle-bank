package dev.stolle.daos;

import java.util.List;

import dev.stolle.models.Account;
import dev.stolle.models.Customer;

public interface TransactionDao {
	public boolean applyForAccount(Customer customer, Account account);
	public boolean approveAccount(Account account);
	public boolean rejectAccount(Account account);
	public boolean updateBalance(Account account);
	public List<Account> getPendingAccounts();
}
