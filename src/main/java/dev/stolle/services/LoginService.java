package dev.stolle.services;

import java.util.Scanner;

import dev.stolle.daos.UserDaoImpl;
import dev.stolle.models.*;

public class LoginService {
	private Scanner scanner = new Scanner(System.in); 
	
	public Customer customerSignIn() {
		System.out.println("Please enter your username.");
		String userName = scanner.next();
		System.out.println("Please enter your password.");
		String password = scanner.next();
		// verify customer 
		UserDaoImpl verifier = new UserDaoImpl();
		Customer customer = verifier.getCustomer(userName, password);
		
		return customer;
	}
	
	public Employee employeeSignIn() {
		System.out.println("Please enter your username.");
		String userName = scanner.next();
		System.out.println("Please enter your password.");
		String password = scanner.next();
		UserDaoImpl verifier = new UserDaoImpl();
		Employee employee = verifier.getEmployee(userName, password);
		
		return employee;
	}
}
