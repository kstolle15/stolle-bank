package dev.stolle;

import java.util.Scanner;

import org.apache.log4j.Logger;

import dev.stolle.models.Customer;
import dev.stolle.models.Employee;
import dev.stolle.services.*;

public class BankDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LoginService login = new LoginService();
		TransactionService transaction = new TransactionService();
		Logger log = Logger.getRootLogger();
		Scanner scanner = new Scanner(System.in); 
		log.info("Starting Application.");
		System.out.println("Are you signing in as a: \nCustomer (1)\nEmployee (2)");
		int userType = Integer.parseInt(scanner.next());
		if (userType == 1) {
			Customer customer = login.customerSignIn();
			transaction.CustomerTransaction(customer);
		} else {
			Employee employee = login.employeeSignIn();
			transaction.EmployeeTransaction(employee);
		} 
		
		
		
		
		
	}

}
