package dev.stolle.models;

public class Employee extends User {
	public Employee() {
		super();
	}
	
	public Employee(int userId,String userName, String password, String firstName, String lastName) {
		super(userId,userName,password,firstName,lastName);
	}
}
