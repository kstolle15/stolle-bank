package dev.stolle.models;

public class Account {
	private int accountNumber;
	private double balance;
	private AccountType type;
	private AccountStatus status;
	
	public Account() {
		super();
	}
	
	public Account(int accountNumber, double balance, AccountType type, AccountStatus status) {
		super();
		setAccountNumber(accountNumber);
		setBalance(balance);
		setType(type);
		setStatus(status);
	}
	
	

	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public AccountType getType() {
		return type;
	}

	public void setType(AccountType type) {
		this.type = type;
	} 
	
	public boolean withdrawal(double amount) {
		if (amount > getBalance() || amount <= 0) {
			return false;
		} else {
			setBalance(getBalance() - amount);
			return true;
		}
	}
	
	public boolean deposit(double amount) {
		if(amount <= 0) {
			return false;
		} else {
			setBalance(getBalance() + amount);
			return true;
		}
	}

	@Override
	public String toString() {
		return "Account [accountNumber=" + accountNumber + ", balance=" + balance + ", type=" + type + ", status="
				+ status + "]";
	}
	
	
}
